# OzGrav Research

This repository is a listing of my time doing research for the OzGrav team at UWA.

OzGrav researches gravitational waves, and attempts to use the data taken from various gravitation wave detectors (primarily LIGO in the US) to determine the direction of various waves so that their sources may be studied. The UWA team has developed a pipeline for analysis called SPIIR (summed parallel infinite impulse response) to provide low latency gravitational wave detection.

My research project primarily focusses on developing methods to filter or ignore specific detectors for detection, but to ensure that those detectors are still synchronized with all the other data that comes in.
To give a proper idea, the below is the data flow for the current pipeline:

![](resources/current_pipeline.png)

The plan is to implement this data flow:

![](resources/new_pipeline.png)

## Components

There will be multiple components of this research repository, and links to all of them will be found in this section.

- [Proposal](https://tommoa.github.io/ozgrav-research/proposal.pdf)
- [Progress Report (2020/05/29)](https://tommoa.github.io/ozgrav-research/progress-2020-05-29.pdf)
- [Callgraph for postprocessing](https://github.com/Tommoa/ozgrav-research/blob/master/resources/callgraph.png)
- [Complexity analysis of postprocessing](https://tommoa.github.io/ozgrav-research/analysis.pdf)

## Building benchmarks

- You can build the benchmarks with:
`make -C benchmarking`
- If you want to use `nvcc` (NVIDIA's CUDA compiler) instead of `clang`, run:
`make -C benchmarking nvcc`

### Dependencies

- CUDA > 9.0
- clang > 8.0
- Intel TBB (`clang` only)
